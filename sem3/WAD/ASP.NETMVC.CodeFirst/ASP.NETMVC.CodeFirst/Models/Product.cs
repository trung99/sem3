﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace ASP.NETMVC.CodeFirst.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        public string name{ get;set;}
        public decimal Price { get; set; }
        public string Decs { get; set; }
        public string Addr { get; set; }
        public DateTime IssuDate { get; set; }
    }
    public class ProductDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

    }
}